# GitLab.com Support Issue Tracker

## **Support for GitLab.com issues only** 

Welcome to the support resource for users of GitLab.com. Issues reported here should only relate to those experienced by users of the GitLab.com Platform.

See [Getting Help](https://about.gitlab.com/getting-help/) for support with self-hosted installations and report GitLab CE bugs in the [GitLab CE Issue Tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues).

Please raise an issue here if you:
- Are experiencing a problem using GitLab.com
- Require advice regarding GitLab.com usage
- Are experiencing a problem when using a service related to GitLab.com
 - Container Registry
 - GitLab.com Pages [https://pages.gitlab.io](https://pages.gitlab.io)

However, for the following issues regarding GitLab.com access or username changes please raise a [support request](https://about.gitlab.com/support/#reaching-support):
- Trouble accessing your account
- Require 2FA to be disabled
- Require a username change and the username is currently occupied by another user or group

### Issue Tracker Policies

Issues in this project are automatically managed. Please see the [.triage-policies.yml](.triage-policies.yml) file to review the current policies

#### Improving Issue triage policies

Our policies are not perfect and always looking to make them better. Please help us to improve them by submitting an issue with your proposals

#### Raising issues by email

For your convenience, we have enabled Service Desk support for this issue tracker. You can create a new issue by sending an email to the following address:
- `incoming+gitlab-com/support-forum { at } gitlab.com`